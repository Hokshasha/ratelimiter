﻿using RateLimiterProject.Converters;
using System.ComponentModel;

namespace RateLimiterProject.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum PropertyType
    {
        /// <summary>
        /// With garden property type.
        /// </summary>
        [Description("With garden")]
        WithGarden,

        /// <summary>
        /// Without garden property type.
        /// </summary>

        [Description("All properties")]
        All
    }
}
