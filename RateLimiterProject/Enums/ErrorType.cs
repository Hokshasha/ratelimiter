﻿namespace RateLimiterProject.Enums
{
    public enum ErrorType
    {
        /// <summary>
        /// Debug error type.
        /// </summary>
        Debug,

        /// <summary>
        /// Error error type.
        /// </summary>
        Error,

        /// <summary>
        /// Info error type.
        /// </summary>
        Info,

        /// <summary>
        /// Warning error type.
        /// </summary>
        Warning
    }
}
