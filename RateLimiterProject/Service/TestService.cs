﻿using RateLimiterProject.Enums;
using RateLimiterProject.Models;
using RateLimiterProject.Serializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace RateLimiterProject.Service
{
    public class TestService
    {
        private static TestService instance;
        private readonly string endPointUrl = "http://partnerapi.funda.nl/feeds/Aanbod.svc/Json/{0}/?type=koop&zo=/{1}{2}/&page={3}&pagesize={4}";
        private readonly string accessKey = "ac1b0b1572524640a0ecc54de453ea9f";
        private readonly string withGardenUrlPart = "/tuin";
        private readonly ThrottleService throttleService;


        private TestService()
        {
            this.throttleService = new ThrottleService(100, TimeSpan.FromMinutes(1));
        }

        /// <summary>
        /// Gets the until instance.
        /// </summary>
        public static TestService Instance => instance ?? (instance = new TestService());

        /// <summary>
        /// Searches for funda for properties.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="city">The city.</param>
        /// <param name="propertyType">The property type</param>
        /// <returns>The search results.</returns>
        public async Task<SearchResult> Search(int pageNumber, string city, PropertyType propertyType, int pageSize = 10)
        {
            string url = String.Format(this.endPointUrl, this.accessKey, city, (propertyType == PropertyType.WithGarden ? this.withGardenUrlPart : ""), pageNumber, pageSize);
            return await this.throttleService.PerformTask<SearchResult>(this.GetHttpResponse(url));
        }

        public async Task<SearchResult> GetAll(string city, PropertyType propertyType)
        {
            // Cannot find the end point to get all at once since it always return maximum 25 
            // Honestly I can't find the reason for that
            // So instead I loop through the pages to get all items in one result

            bool continueLoop = true;
            int pageNumber = 1;
            SearchResult result = new SearchResult();
            List<Property> properties = new List<Property>();
            do
            {
                result = await this.Search(pageNumber++, city, propertyType, 25);

                if (result != null)
                {
                    properties.AddRange(result.Objects);
                    continueLoop = properties.Count != result.TotaalAantalObjecten;
                }
                else
                {
                    continueLoop = false;
                }
            } while (continueLoop);

            if (result != null)
            { 
                result.Objects = properties;
                result.Paging.AantalPaginas = 1;
                result.Paging.HuidigePagina = 1;
            }

            return result;
        }

        /// <summary>
        /// Gets https reponse.
        /// </summary>
        /// <typeparam name="T">The type.</typeparam>
        /// <param name="url">The request url.</param>
        /// <returns>The return type.</returns>
        private async Task<SearchResult> GetHttpResponse(string url)
        {
            try
            { 
                WebRequest request = HttpWebRequest.Create(url);
                request.Method = "GET";
                using (WebResponse response =  await request.GetResponseAsync())
                {
                    using (Stream messagestream = response.GetResponseStream())
                    {
                        var serializer = new CustomJsonSerializer<SearchResult>();
                        return serializer.Deserialize(messagestream);
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
