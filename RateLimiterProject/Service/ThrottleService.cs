﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace RateLimiterProject.Service
{
    public class ThrottleService
    {
        private readonly SemaphoreSlim semaphore;
        private readonly Timer tasksTimer;
        private int numberOfCompeletedRequests;
        private int totalNumberOfRequest;

        public ThrottleService(int numberOfRequest, TimeSpan timeUnit)
        {
            this.totalNumberOfRequest = numberOfRequest;
            this.semaphore = new SemaphoreSlim(this.totalNumberOfRequest, this.totalNumberOfRequest);
            this.tasksTimer = new Timer((int)timeUnit.TotalMilliseconds);
            this.tasksTimer.Elapsed += this.TaskTimerElapsed;
            this.tasksTimer.AutoReset = true;
            this.tasksTimer.Enabled = true;

            // used to have a value shared between threads.
            Interlocked.Exchange(ref this.numberOfCompeletedRequests, 0);
        }

        public async Task<T> PerformTask<T>(Task<T> taskToPreform)
        {
            try
            {
                await this.WaitToProceed();
                T taskOutput = await taskToPreform;
                await Task.Delay(100);
                return taskOutput;
            }
            finally
            { 
                this.TaskCompleted();
            }
        }

        private void TaskTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.semaphore.Release(Interlocked.Exchange(ref this.numberOfCompeletedRequests, 0));
        }

        private void TaskCompleted()
        {
            Interlocked.Increment(ref this.numberOfCompeletedRequests);
        }
        
        private async Task<bool> WaitToProceed()
        {
            return await this.semaphore.WaitAsync(Timeout.Infinite);
        }
    }
}
