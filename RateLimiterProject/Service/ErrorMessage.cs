﻿using RateLimiterProject.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateLimiterProject.Service
{
    /// <summary>
    /// Error Message
    /// </summary>
    public class ErrorMessage
    {
        // Constructor

        public ErrorMessage(object sender, string message, ErrorType errorType)
        {
            this.Sender = sender;
            this.Message = message;
            this.ErrorType = errorType;
        }

        // Properties

        /// <summary>
        /// Gets the warning message.
        /// </summary>
        /// <value>
        /// The warning message.
        /// </value>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the sender.
        /// </summary>
        /// <value>
        /// The sender.
        /// </value>
        public object Sender { get; private set; }

        /// <summary>
        /// Gets the type of the error.
        /// </summary>
        /// <value>
        /// The type of the error.
        /// </value>
        public ErrorType ErrorType { get; private set; }
    }
}
