﻿using RateLimiterProject.ViewModels;
using System.Windows;

namespace RateLimiterProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainWindowViewModel viewModel;

        public MainWindow(MainWindowViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.InitializeComponent();
            this.DataContext = viewModel;
        }
    }
}
