﻿using RateLimiterProject.Serializers.JsonConverters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateLimiterProject.Serializers
{
    public static class CustomJsonSerializerSettings
    {  /// <summary>
       /// Gets the default.
       /// </summary>
       /// <value>
       /// The default.
       /// </value>
        public static JsonSerializerSettings Default
        {
            get
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    TypeNameHandling = TypeNameHandling.Auto
                };

                settings.Culture = CultureInfo.InvariantCulture;
                settings.Converters.Add(new MetadataJsonConverter());
                settings.Converters.Add(new PropertyJsonConverter());
                settings.Converters.Add(new PagingJsonConverter());
                settings.Converters.Add(new SearchResultJsonConverter());

                return settings;
            }
        }
   }
}
