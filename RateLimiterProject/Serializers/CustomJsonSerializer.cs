﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateLimiterProject.Serializers
{
    /// <summary>
    ///     Serializes and deserializes T to JSON
    /// </summary>
    /// <typeparam name="T">a class</typeparam>
    /// <seealso cref="DataTypes.Interfaces.ICustomSerializer{T}" />
    public class CustomJsonSerializer<T> : ICustomSerializer<T>
        where T : class
    {
        // Constructor
        static CustomJsonSerializer()
        {
        }

        /// <summary>
        /// Serializes the specified serializable.
        /// </summary>
        /// <param name="serializable">The serializable.</param>
        /// <param name="stream">The stream.</param>
        public void Serialize(T serializable, Stream stream)
        {
            // Serailze
            string serialized = JsonConvert.SerializeObject(serializable, CustomJsonSerializerSettings.Default);

            // Save to file
            using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8, 1024, true))
            {
                writer.Write(serialized);
            }
        }

        /// <summary>
        /// Deserializes the specified stream.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>
        /// Type object
        /// </returns>
        public T Deserialize(Stream stream)
        {
            if (stream.CanSeek)
            {
                stream.Seek(0, SeekOrigin.Begin);
            }

            using (StreamReader reader = new StreamReader(stream))
            {
                string streamString = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(streamString, CustomJsonSerializerSettings.Default);
            }
        }

        public T Deserialize(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<T>(input, CustomJsonSerializerSettings.Default);

        }
    }
}
