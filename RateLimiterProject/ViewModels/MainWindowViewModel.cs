﻿using RateLimiterProject.Enums;
using RateLimiterProject.Models;
using RateLimiterProject.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToastNotifications.Messages;

namespace RateLimiterProject.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private SearchResult searchResult;
        private RelayCommand searchCommand;
        private RelayCommand getTop10Command;
        private RelayCommand previousPageCommand;
        private RelayCommand nextPageCommand;
        private string city = "Amsterdam";
        private PropertyType propertyType;
        private IEnumerable<string> topMakelaars;
        private bool isPreviousPageButtonEnabled;
        private bool isNextPageButtonEnabled;
        private bool isUpdating = false;

        /// <summary>
        /// Constructor.
        /// </summary>
        public MainWindowViewModel()
        { }

        /// <summary>
        /// Gets or sets the property type.
        /// </summary>
        public PropertyType PropertyType
        {
            get => this.propertyType;
            set
            {
                this.propertyType = value;
                this.RaisePropertyChanged(nameof(this.PropertyType));
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is the first page.
        /// </summary>
        public bool IsPreviousPageButtonEnabled
        {
            get => this.isPreviousPageButtonEnabled;
            private set
            {
                this.isPreviousPageButtonEnabled = value;
                this.RaisePropertyChanged(nameof(this.IsPreviousPageButtonEnabled));
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether it is the last page.
        /// </summary>
        public bool IsNextPageButtonEnabled
        {
            get => this.isNextPageButtonEnabled;
            private set
            {
                this.isNextPageButtonEnabled = value;
                this.RaisePropertyChanged(nameof(this.IsNextPageButtonEnabled));
            }
        } 
        
        /// <summary>
        /// Gets or sets a value indicating whether it is the last page.
        /// </summary>
        public bool IsUpdating
        {
            get => this.isUpdating;
            private set
            {
                this.isUpdating = value;
                this.RaisePropertyChanged(nameof(this.IsUpdating));
            }
        }
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City
        {
            get => this.city;
            private set
            {
                this.city = value;
                this.RaisePropertyChanged(nameof(this.City));
            }
        }

        /// <summary>
        /// Gets or sets the search result.
        /// </summary>
        public SearchResult SearchResult
        {
            get => this.searchResult;
            private set
            {
                this.searchResult = value;
                this.RaisePropertyChanged(nameof(this.SearchResult));
            }
        }
       
        /// <summary>
        /// Gets or sets the top makelaars.
        /// </summary>
        public IEnumerable<string> TopMakelaars
        {
            get => this.topMakelaars;
            set
            {
                this.topMakelaars = value;
                this.RaisePropertyChanged(nameof(this.TopMakelaars));
            }
        }

        /// <summary>
        /// Gets the get top 10 command.
        /// </summary>
        public RelayCommand GetTop10MakelaarsCommand
        {
            get => this.getTop10Command ?? (this.getTop10Command = new RelayCommand(this.GetTop10Makelaars));
        }


        /// <summary>
        /// Gets search command.
        /// </summary>
        public RelayCommand SearchCommand
        {
            get => this.searchCommand ?? (this.searchCommand = new RelayCommand(this.Search));
        }

        /// <summary>
        /// Gets previous page command.
        /// </summary>
        public RelayCommand PreviousPageCommand
        {
            get => this.previousPageCommand ?? (this.previousPageCommand = new RelayCommand(this.PreviousPage));
        }

        /// <summary>
        /// Gets next page command.
        /// </summary>
        public RelayCommand NextPageCommand
        {
            get => this.nextPageCommand ?? (this.nextPageCommand = new RelayCommand(this.NextPage));
        }

        private async void GetTop10Makelaars()
        {
            this.IsUpdating = true;
            this.SearchResult = await TestService.Instance.GetAll(this.city, this.propertyType);
            this.TopMakelaars = this.SearchResult?.FilterTopMakelaar(10);
            this.HandleError();
            this.IsUpdating = false;
        }

        private async void Search()
        {
            this.IsUpdating = true;
            this.SearchResult = await TestService.Instance.Search(1, this.city, this.propertyType);
            this.HandleError();
            this.IsUpdating = false;
        }

        private async void PreviousPage()
        {
            if (this.searchResult != null &&  this.SearchResult.Paging.HuidigePagina > 0)
            {
                this.IsUpdating = true;
                this.SearchResult = await TestService.Instance.Search(this.SearchResult.Paging.HuidigePagina - 1, this.city, this.propertyType);
                this.HandleError();
                this.IsUpdating = false;
            }
        }

        private async void NextPage()
        {
            if (this.searchResult != null && this.SearchResult.Paging.AantalPaginas > this.SearchResult.Paging.HuidigePagina)
            {
                this.IsUpdating = true;
                this.SearchResult = await TestService.Instance.Search(this.SearchResult.Paging.HuidigePagina + 1, this.city, this.propertyType);
                this.HandleError();
                this.IsUpdating = false;
            }
        }

        private void HandleError()
        {
            if (this.SearchResult == null)
            {
                this.IsNextPageButtonEnabled = false;
                this.IsPreviousPageButtonEnabled = false;
                App.Current.Notifier.ShowError("An error occured handling your request");

            }
            else
            {
                this.ToggleButtons();
            }
        }

        private void ToggleButtons()
        {
            this.IsPreviousPageButtonEnabled = this.SearchResult.Paging.HuidigePagina > 1;
            this.IsNextPageButtonEnabled = this.SearchResult.Paging.AantalPaginas > this.SearchResult.Paging.HuidigePagina;
        }

    }
}
