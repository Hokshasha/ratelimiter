﻿using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;

namespace RateLimiterProject.Ioc
{
    class Locator
    {
        static Locator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
          
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <returns>Object.</returns>
        public static T GetInstance<T>()
        {
            return SimpleIoc.Default.GetInstance<T>();
        }

        /// <summary>
        ///     Gets the instance.
        /// </summary>
        /// <typeparam name="T">The type you want to get from the locator</typeparam>
        /// <param name="key">The key.</param>
        /// <returns>Generic instance</returns>
        public static T GetInstance<T>(string key)
        {
            return SimpleIoc.Default.GetInstance<T>(key);
        }
    }
}
