﻿using RateLimiterProject.Enums;
using RateLimiterProject.Service;
using RateLimiterProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace RateLimiterProject

{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        // Fields



        // Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="App" /> class.
        /// </summary>
        public App()
        {
        }

        // Properties

        /// <summary>
        ///     Gets the current.
        /// </summary>
        /// <value>
        ///     The current.
        /// </value>
        public static new App Current => Application.Current as App;

        /// <summary>
        ///     Gets the MainViewModel.
        /// </summary>
        public MainWindowViewModel MainViewModel { get; private set; }

        /// <summary>
        ///     Gets or sets The notifier
        /// </summary>
        public Notifier Notifier { get; set; } = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.BottomRight,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(5),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.DisplayOptions.TopMost = false;
            cfg.DisplayOptions.Width = 250;
            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        private void InitializeMainWindow()
        {
            this.MainWindow = new MainWindow(this.MainViewModel);
            this.MainWindow.Closing += this.MainWindow_Closed;
            this.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            this.MainWindow.Show();
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            // Shutdown application
            Environment.Exit(0);
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            this.MainViewModel = new MainWindowViewModel();
            this.InitializeMainWindow();
        }
    }
}
