﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateLimiterProject.Models
{
    public class Paging
    {
        /// <summary>
        /// Gets or sets the AantalPaginas.
        /// </summary>
        public int AantalPaginas { get; set; }

        /// <summary>
        /// Gets or sets the HuidigePagina.
        /// </summary>
        public int HuidigePagina { get; set; }

        /// <summary>
        /// Gets or sets the VolgendeUrl.
        /// </summary>
        public string VolgendeUrl { get; set; }
    }
}
