﻿using System;
namespace RateLimiterProject.Models
{
    public class Property
    {
        /// <summary>
        /// Gets or sets the AangebodenSinds tekst
        /// </summary>
        public string AangebodenSindsTekst { get; set; }

        /// <summary>
        /// Gets or sets the foto.
        /// </summary>
        public string Foto { get; set; }

        /// <summary>
        /// Gets or sets the guid.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the Makelaar Naam.
        /// </summary>
        public string MakelaarNaam { get; set; }

        /// <summary>
        /// Gets or sets the MakelaarId.
        /// </summary>
        public int MakelaarId { get; set; }

        /// <summary>
        /// Gets or sets the Url.
        /// </summary>
        public string URL { get; set; }
    }
}
