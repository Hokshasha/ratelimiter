﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateLimiterProject.Models
{
    public class SearchResult
    {
        /// <summary>
        /// Gets or sets the TotaalAantalObjecten.
        /// </summary>
        public int TotaalAantalObjecten { get; set; }

        /// <summary>
        /// Gets or sets the metadata.
        /// </summary>
        public Metadata Metadata { get; set; }

        /// <summary>
        /// Gets or sets the paging.
        /// </summary>
        public Paging Paging { get; set; }
        
        /// <summary>
        /// Gets or sets the list of properties
        /// </summary>
        public List<Property> Objects { get; set; }

        public IEnumerable<string> FilterTopMakelaar(int makelaarNumber)
        {
           IEnumerable<string> topMakelaarIds = this.Objects.GroupBy(o => o.MakelaarNaam + ", Id:" + o.MakelaarId)
                                            .OrderByDescending(o => o.Count())
                                            .Select(s => s.Key).Take(makelaarNumber);

            return topMakelaarIds;
        }
    }
}
