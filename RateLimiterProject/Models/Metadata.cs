﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateLimiterProject.Models
{
    public class Metadata
    {
        /// <summary>
        /// Gets or sets the object type.
        /// </summary>
        public string ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the omschrijving.
        /// </summary>
        public string Omschrijving { get; set; }

        /// <summary>
        /// Gets or set the titel.
        /// </summary>
        public string Titel { get; set; }
    }
}
